var app = require('app');
var BrowserWindow = require('browser-window');
var ipc = require('ipc');

require('crash-reporter').start();

var mainWindow = null;

app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', function() {
    mainWindow = new BrowserWindow({
      width: 1000,
      height: 600,
      frame : false
    });
    mainWindow.loadUrl('file://' + __dirname + '/index.html');

    ipc.on('close-win', function(event, status) {
      mainWindow.close();
    });

    ipc.on('minimize-win', function(event, status) {
      mainWindow.minimize();
    });

    ipc.on('maximize-win', function(event, status) {
      mainWindow.maximize();
    });

    mainWindow.on('closed', function() {
        mainWindow = null;
    });
});
