(function () {

    var ipc = require('ipc');

    function init() {
        window.document.getElementById("win_close").addEventListener("click", function (e) {
            ipc.send('close-win',true);
        });

        window.document.getElementById("win_minimize").addEventListener("click", function (e) {
            ipc.send('minimize-win',true);
        });

        window.document.getElementById("win_fullscreen").addEventListener("click", function (e) {
            ipc.send('maximize-win',true);
        });
    };

    document.onreadystatechange = function () {
        if (document.readyState == "complete") {
            init();
        }
    };

})();
